# 自定义中间件类

from django.utils.deprecation import MiddlewareMixin

# 过根据每个中间件调用，有一个拦截掉直接返回了后面的中间件不会被调用
# 类似于 java 的拦截器
def process_view(request, view_func, view_args, view_kwargs):
    print("md1  process_view 方法！") #在视图之前执行 顺序执行
    #return view_func(request)


def process_request(request):
    print("md1  process_request 方法。", id(request)) #在视图之前执行


class MD1(MiddlewareMixin):

    def process_response(self,request, response):
        print("md1  process_response 方法！", id(request)) #在视图之后


