from django.http import HttpResponse
from django.contrib.auth.models import User
from model.models import t_user


def userAuth(request):
    # 创建普通用户，密码是密文
    u = User.objects.create_user(username='runboo', password='123456')
    return HttpResponse(u)


def testInsert(request):
    test1 = t_user()
    test1.username = '李四'
    test1.save()
    return HttpResponse("<p>数据添加成功！</p>")


def testUpdate(request):
    test1 = t_user.objects.get(id=1)
    test1.username = '张三'
    test1.save()

    # 另外一种方式
    #Test.objects.filter(id=1).update(username='Google')
    # 修改所有的列
    # Test.objects.all().update(username='Google')

    return HttpResponse("<p>数据更新成功！</p>")


def testDelete(request):
    test1 = t_user.objects.get(id=1)
    test1.delete()

    # 另外一种方式
    # Test.objects.filter(id=1).delete()
    # 删除所有数据
    # Test.objects.all().delete()

    return HttpResponse("<p>数据更新成功！</p>")


def testSelect(request):
    # 初始化
    response = ""
    response1 = ""
    # 通过objects这个模型管理器的all()获得所有数据行，相当于SQL中的SELECT * FROM
    list = t_user.objects.all()

    # filter相当于SQL中的WHERE，可设置条件过滤结果
    response2 = t_user.objects.filter(id=1)

    # 获取单个对象
    response3 = t_user.objects.get(id=1)

    # 限制返回的数据 相当于 SQL 中的 OFFSET 0 LIMIT 2;
    t_user.objects.order_by('id')[0:10]

    # 数据排序
    t_user.objects.order_by("id")
    print("--------------")
    print(list)
    print("==============")

    # 上面的方法可以连锁使用
    # t_user.objects.filter(username="runoob").order_by("id")

    # 输出所有数据
    for var in list:
        response1 += str(var.id) + ' -> ' + var.username + '<br>'
    response = response1
    return HttpResponse("<p>" + response + "</p>")
