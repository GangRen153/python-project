"""
ASGI 相关配置，ASGI 是 Django 3.0 版本的一个新特性
在 Django 3.0 版本中支持 ASGI 服务器器，在这之前是 WSGI
具体 ASGI 是什么，讲解起来比较深奥难懂
可以了解 ASGI 、WSGI、CGI ，将它们对比起来学习。

ASGI config for demo project.

It exposes the ASGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/4.0/howto/deployment/asgi/
"""

import os

from django.core.asgi import get_asgi_application

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'demo.settings')

application = get_asgi_application()
