from django.http import HttpResponse
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt


def hello(request):
    return HttpResponse("Hello world ! ")


# view
def view(request):
    context = {
        'hello': 'Hello World! view mode html',
        'arr_list': ['菜鸟教程1', '菜鸟教程2', '菜鸟教程3'],
        'object': {'name': '张三', 'age': 19}
    }
    return render(request, 'view.html', context)


# 接收请求数据
def searchGet(request):
    request.encoding = 'utf-8'
    if 'name' in request.GET and request.GET['name']:
        message = '你搜索的内容为: ' + request.GET['name']
    else:
        message = '你提交了空表单'
    return HttpResponse(message)


def searchPost(request):
    ctx = {}
    if request.POST:
        ctx['name'] = request.POST['name']
        ctx['content'] = request.POST['content']
    return HttpResponse(ctx)


@csrf_exempt
def searchPost2(request):
    ctx = {}
    request.encoding = 'utf-8'
    s = request.body
    print(type(s))
    print(s)
    return HttpResponse("菜鸟教程 post body")
