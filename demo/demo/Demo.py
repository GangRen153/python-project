# 测试 demo 脚本
# 基础语法
# 单行注释
'''
:param:多行注释
'''
import requests
import simplejson as simplejson

"""
:param:多行注释
"""

if 1 == 2:
    print(1)
elif 2 != 2:
    print(2)

total = "item_one" + \
        "item_two" + \
        "item_three"
# print(total)


import sys;  # 引入指定模块
from math import *  # 引入所有模块

# https://www.runoob.com/python/python-strings.html 字符串相关函数处理
x = 'sdsd';
# 写出
# sys.stdout.write(x + '\n');

x, y, z = 1, 1.0, 'string'
# print(x, y, z)

var1 = 1
var2 = 10
# print(var1)
# 删除对象，删除之后就没了，相当于null
# del [var1,var2]
# del var1, var2

# 截取字符串，下标从0开始
string = "abcdef"
strs = string[1:3]  # 截取字符串并用新的变量接收
# print(string[1:3]) # 截取字符串
# print(string[1:3].__len__()) # 输出截取后字符串长度
# print(strs * 2) # * 重复输出

list = ["a", "b", "c", "d", "e"]
list.append('f')  # 追加
# print(list)
list2 = ['runoob', 786, 2.23, 'john', 70.2]
# print(list[0])        # 输出第0个元素
# print(list[1:3])      # 输出第1个元素和第2个元素，不包括第3位置的元素
# list2 = list * 2      # 合并两次数组
# list2 = list + list2  # 合并两个数组
# print(list2)          # 打印组数合并n次后的结果

# python 元组，类似 list，写法同数组一模一样，更像 object[]
tuple = ('runoob', 786, 2.23, 'john', 70.2)
tinytuple = (123, 'john')
# print(tuple + tinytuple)

# 字典（字典是通过键取值的，相当于 map 结构）
dict = {}
dict['one'] = "This is one"
dict[2] = "This is two"
# print(dict['one'])
# print(dict[2])

tinydict = {'name': 'runoob', 'code': 6734, 'dept': 'sales'}
# print(tinydict.get('name'))   # 获取key值，相当于 java get
# print(tinydict.pop('name'))   # 弹出key值，相当于 java remove
# print(tinydict.keys())        # 拿到所有 key
# print(tinydict.values())      # 拿到所有 value
import time

# print(time.time())            # 打印当前时间
# print(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime()))                     # 时间转字符串
# print(time.mktime(time.strptime("2022-01-01 12:01:19","%Y-%m-%d %H:%M:%S")))    # 字符串转时间戳


# 数据类型转换
# print(int('4'))
# print(int("3.9"))             # 报错，只有字符串整数才能转成int，类型匹配不上不能转
# print(int(3.9))               # 不报错

# print(complex(1,30))          # 输出 (1+30j)，30j 表示浮点数
dict = {'runoob': 'runoob.com', 'google': 'google.com'}  # 对象
# print(str(dict))              # 参数是 object 类型，返回对象的 string 格式 {'runoob': 'runoob.com', 'google': 'google.com'}
# print(str(dict))              # 将对象 object 转换为表达式字符串 {'runoob': 'runoob.com', 'google': 'google.com'}
# print(eval("{'runoob': 'runoob.com'}")) # 将string转成对象，相当于json转对象（json格式错误则报错）
# print(tuple([123, 'xyz', 'zara', 'abc']))        # 将列表转成元组，报错
# print(chr(100))               # 将数字转成 char
# print(unichr(1))              # 报错
# print(ord('1'))               # 将数值传成字符
# print(hex(100))               # 将数字转成16进制
# print(oct(100))               # 将数字转成8进制

# while 循环
i = 0
while (i < 5):
    i = i + 1
    if i % 2 > 0:
        continue
    if i == 4:
        break
    # print(i)
# else:
# print("i >= 5 了，while 结束")

# for 循环
for letter in 'python':
    break
    print(letter)
for letter in ['java', 'python', 'go', 'c', 'c++']:
    break
    print(letter)
for index in range(len(['java', 'python', 'go', 'c', 'c++'])):
    break
    print(index)
for letter in 'Python':
    if letter == 'h':
        pass  # 占位符，好像没啥意义
        # print('这是 pass 块')
    # print('当前字母 :', letter)


# 严格参数类型和参数顺序
def test(age):
    # print(age)
    # print("进入test方法")
    return


test(1)


# 可以设置参数默认值，传递参数时可以不传
def printinfo(name, age=18):
    "打印任何传入的字符串"
    # print("Name: ", name)
    # print("Age ", age)
    return


printinfo("miki")

# *vartuple：相当于 java 的 String... strs
def printparams(args, *vartuple):
    print(args)
    for var in vartuple:
        print(var)
    return

# **vartuple：表示字典序参数，相当于 java 的 Map<String,String> ... strs
def printparams2(args, **vartuple):
    print(args)
    print(vartuple)

# printparams(1,2,3)
d = {'key1' : 'value1', 'key2' : 'value2', 'key3' : 'value3' }
# printparams2(1, d) # 报错

# 文件读写
try:
    fo = open("C:\\Users\\钢人\\Desktop\\新建文本文档.txt", "w")
    # fo.writelines("册数数是电脑辐射\n")
    # fo.writelines("卡死防守对方的\n")
    # fo.close()

    object = open("C:\\Users\\钢人\\Desktop\\新建文本文档.txt", "r+")
    # print(object.read(10))
    # object.close()
except IOError:
    print("IO异常")
except([IndexError, UnicodeError]):
    print("IndexError、UnicodeError 异常")
# else:
#    print("其他异常")
finally:
    fo.close()
    object.close()
    # raise RuntimeError("自定义触发异常")


# 自定义类对象
class Employee1:
    public = "公开变量"
    _protected = "只能本身或子类访问"
    __private = "只能当前类访问"

    def employee11(self):
        # print("进入自定义的类11：" + self.__private)
        return


class Employee2:
    def employee22(self):
        # print("进入自定义的类22")
        return


class Child(Employee1, Employee2):  # 继承关系 Child 继承的 Employee
    def child1(self):
        # print("childTest")
        return

    def employee11(self):
        # print("重写父类方法")
        return

    def __method__(self):
        # print("私有方法")
        return


# 类对象调用
e = Child()
e.__method__()  # 父类的私有方法子类实现不能访问，只能访问子类自己的私有方法

# 多线程
import _thread


def print_thread(threadName, delay):
    return
    print(threadName)


# 创建两个线程
try:
    _thread.start_new_thread(print_thread, ("Thread-1", 2,))
    _thread.start_new_thread(print_thread, ("Thread-2", 4,))
except:
    print("Error: 无法启动线程")

#while 1:
#    pass

import threading
import time

# 表示继承自Thread对象
class myThread (threading.Thread):
    def __init__(self, threadID, name, delay):
        threading.Thread.__init__(self)
        self.threadID = threadID
        self.name = name
        self.delay = delay
    def run(self):
        print ("开启线程： " + self.name)
        # 获取锁，用于线程同步
        threadLock.acquire()
        print_time(self.name, self.delay, 3)
        # 释放锁，开启下一个线程
        threadLock.release()

def print_time(threadName, delay, counter):
    while counter:
        time.sleep(delay)
        print ("%s: %s" % (threadName, time.ctime(time.time())))
        counter -= 1

threadLock = threading.Lock()
threads = []

# 创建新线程
thread1 = myThread(1, "Thread-1", 1)
thread2 = myThread(2, "Thread-2", 2)

# 开启新线程
#thread1.start()
#thread2.start()

# 添加线程到线程列表
#threads.append(thread1)
#threads.append(thread2)

# 等待所有线程完成
#for t in threads:
#   t.join()
#print ("退出主线程")

import json
data1 = {
    'no' : 1,
    'name' : 'Runoob',
    'url' : 'http://www.runoob.com'
}

jsonStr = json.dumps(data1)                 # 对象转 json
#print("object -> json string:" + jsonStr)
data = json.loads("{\"no\": 1, \"name\": \"Runoob\", \"url\": \"http://www.runoob.com\"}")  # json 转对象
#print(data)

# 读取json文件，进行读写操作
#with open('data.json', 'w') as f:   # 写入 JSON 数据
#    json.dump(data, f)
#with open('data.json', 'r') as f:   # 读取数据
#    data = json.load(f)

from urllib.request import urlopen
myURL = urlopen("https://www.runoob.com/")
#print(myURL.read())

#import requests
# x = requests.get("https://www.runoob.com/")
#print(x.text)   # 内容
#print(x.status_code)
#print(x.encoding)
#print(x.apparent_encoding)
#print(x.content)
#print(x.ok)    # boolean类型
#print(x.reason)
#print(x.headers)
#print(x.cookies)
#print(x.history)

body = json.dumps({'brand': '12345', 'model': 'test'})
headers = {"Content-Type": "application/json"}
#req = requests.post("http://localhost:8080/home/post", headers=headers, data=None, json=body)
#print(req.status_code)
#print(req.content)

jsonObj = {'brand': '12345', 'model': 'test'}
headers = {'content-type': 'application/json'}
resp = requests.post('http://localhost:8080/home/post', json=jsonObj, headers=headers)
print(resp.status_code)
print(resp.content)


