import requests
import base64

API_KEY = 'OOJIEQQgnCqUThvA807IjqSf'
SECRET_KEY = 'X2RdS9HOaXbz3NdVVCC7oWUPvQXKM0c6'


def get_access_token():
    """
    使用 AK，SK 生成鉴权签名（Access Token）
    :return: access_token，或是None(如果错误)
    """
    params = {"grant_type": "client_credentials", "client_id": API_KEY, "client_secret": SECRET_KEY}
    response = requests.post('https://aip.baidubce.com/oauth/2.0/token', params=params)
    return str(response.json().get("access_token"))


def get_access_words(img_url):
    url = "https://aip.baidubce.com/rest/2.0/ocr/v1/accurate_basic?access_token=" + get_access_token()
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Accept': 'application/json'
    }
    f = open(img_url, 'rb')
    img = base64.b64encode(f.read()).decode('utf-8')
    params = {'image': img}
    response = requests.request("POST", url, headers=headers, data=params)
    obj = response.json()
    return obj.get('words_result')[0].get('words')
