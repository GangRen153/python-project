import json

import pymysql
import ocr
import requests
from PIL import Image
from selenium import webdriver
from selenium.webdriver.firefox.service import Service
from selenium.webdriver.common.by import By
import time
from bs4 import BeautifulSoup

num = 0
success = []
car_types = []
cookie_str = ''


def read_cookie_file():
    global cookie_str
    with open('cookie.txt', 'r', encoding='utf-8') as file:
        cookie_str = file.read()


def read_car_type(create_time='2024-07-16'):
    db_params = {
        'host': '140.143.235.199',
        'user': 'root',
        'password': '1qazxsw23edcvfr4!',
        'db': 'certificate',
        'charset': 'utf8mb4'
    }
    # 连接MySQL
    conn = pymysql.connect(**db_params)
    cursor = conn.cursor()
    # 执行查询语句
    query = f"SELECT clxh FROM hgz_certificate_data WHERE clxh IS NOT NULL AND create_time > '{create_time}'"
    cursor.execute(query)

    # 获取查询结果
    rows = cursor.fetchall()

    # 遍历每一行
    for row in rows:
        car_types.append(row[0])

    try:
        cursor.close()
        conn.close()
    except:
        print('释放mysql连接异常')


def login():
    # 设置火狐浏览器选项
    global cookie_str
    options = webdriver.FirefoxOptions()
    options.add_argument('start-maximized')
    options.add_argument('--proxy-server=http://221.131.165.73:27390')
    options.add_argument("Zoom 100%")
    options.add_argument("Proxy-Authorization=\"Basic ZmFuY2hhbmdzb25nOmx6ZzF5djZn\"")
    options.add_argument(
        "user-agent=Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/58.0.3029.110 Safari/537.3")
    options.add_argument("disable-java")
    options.add_argument("no-sandbox")

    # 初始化webdriver
    service = Service(
        executable_path=r'C:\GangRen\install\tools\Mozilla Firefox\geckodriver-v0.34.0-win64\geckodriver.exe',
        service_url='http://gk.vecc.org.cn/login')
    driver = webdriver.Firefox(service=service)  # 替换为你的geckodriver路径
    driver.maximize_window()
    # driver.execute_script('document.body.style.zoom="0.8"')
    # 打开登录页面
    driver.get('http://gk.vecc.org.cn/login')

    element = driver.find_element(by=By.XPATH, value='/html/body/div/div/div[2]/div/form/div[4]/div/img')
    left = int(element.location['x'])
    top = int(element.location['y'])
    right = int(element.location['x'] + element.size['width'])
    bottom = int(element.location['y'] + element.size['height'])

    img_url = 'C:\GangRen\Python\personalProject\python-project\demo\debug\kaptcha.png'
    driver.save_screenshot(img_url)
    im = Image.open(img_url)
    im = im.crop((left, top, right, bottom))
    im.save(img_url)

    # 模拟输入用户名和密码
    kaptcha = ocr.get_access_words(img_url=img_url)
    driver.find_element(by=By.ID, value='code').send_keys('73013976-8')
    driver.find_element(by=By.ID, value='name').send_keys('JZEPbs')
    driver.find_element(by=By.ID, value='password').send_keys('JZ3795775-b')
    driver.find_element(by=By.NAME, value='kaptcha').send_keys(kaptcha)

    # 模拟点击登录按钮
    login_btn = driver.find_element(By.XPATH, '/html/body/div/div/div[2]/div/form/input[2]')
    login_btn.click()

    # 等待登录过程完成，可能需要自定义等待时间
    time.sleep(5)  # 示例中使用了固定的5秒，实际应根据网站响应时间调整
    current_url = driver.current_url
    # 获取cookie
    cookies = driver.get_cookies()

    # 打印所有cookie
    for cookie in cookies:
        if cookie.get('name') != 'JSESSIONID':
            continue
        cookie_str = f"JSESSIONID={cookie.get('value')}; ucode=73013976-8; uname=JZEPbs"

    print(f'打印cookie -> {cookie_str}')
    with open('cookie.txt', 'w', encoding='utf-8') as file:
        file.write(cookie_str)
    # 关闭浏览器
    driver.quit()


def debug():
    global num
    headers = {
        'X-Requested-With': 'XMLHttpRequest',
        'Referer': 'http://gk.vecc.org.cn/ergs/o3/open/protect',
        'Host': 'gk.vecc.org.cn',
        'Proxy-Authorization': 'Basic ZmFuY2hhbmdzb25nOmx6ZzF5djZn',
        'User-Agent': 'Apifox/1.0.0 (https://apifox.com)',
        'Accept': '*/*',
        'Connection': 'keep-alive',
        'Cookie': 'JSESSIONID=9C38C8B4DD80F2E4BF20B04E4AE8285B'
    }

    for car_type in car_types:
        time.sleep(2)
        num = num + 1
        body = {}
        detailHtmls = []
        url = f"http://gk.vecc.org.cn/ergs/o3/open/protectTable?condition={car_type}&kaptcha=&pageSize=200&pageNum=0&_={int(round(time.time() * 1000))}"
        response = requests.request("GET", url, headers=headers, data={}, files={})

        if response.status_code != 200:
            continue
        soup = BeautifulSoup(response.text, 'lxml')
        table = soup.find('table')
        body['tableHtml'] = f'<html><body>{response.text}</body></html>'
        for row in table.find_all('tr')[1:]:
            # body['tableHtml'] = f'<html><body>{str(row)}</body></html>'
            env_code = row.find_all('td')[0].get_text()
            if env_code.find('CN ') != -1 and env_code.find(' G5 ') != -1:
                continue
            for col in row.find_all('td'):
                a_tag = col.find_all('a')
                if a_tag is None or len(a_tag) == 0:
                    continue
                detailUrl = f'https://gk.vecc.org.cn{a_tag[0].get("href")}'
                headers['Cookie'] = cookie_str
                # detailUrl = r'http://gk.vecc.org.cn/ergs/o3/infoOpen/preview?sbbh
                # =85b7de1045e7d255115e51627fa01871efd519426e7bb918&isprotect=false'
                response = requests.request("GET", detailUrl, headers=headers, data={}, files={})
                time.sleep(3)
                if response.status_code != 200:
                    login()
                    headers['Cookie'] = cookie_str
                    time.sleep(3)
                    response = requests.request("GET", detailUrl, headers=headers, data={}, files={})

                if response.status_code != 200:
                    break
                detailHtmls.append(response.text.replace('\r\n', ''))
                break
            body['detailHtmls'] = detailHtmls
            if len(detailHtmls) == 0:
                continue
            # body['tableHtml'] = f'<html><body><table>{str(title_tag)}{str(row)}<table></body></html>'
            sync_car_type(car_type, body)


def sync_car_type(car_type, body):
    url = "https://zsgl.site:80/certificate/client/environmentalData/offLineSynHb"
    # url = "http://localhost:9090/certificate/client/environmentalData/offLineSynHb"
    payload = json.dumps(body)
    # print(f'打印body \n {payload} \n')
    headers = {
        'token': '5a54d4e5db591de3b676952ef77419c9',
        'Authorization': 'Bearer eyJhbGciOiJIUzUxMiJ9'
                         '.eyJsb2dpbl91c2VyX2tleSI6ImJlYzkwMjhjLTllZTMtNDU2Yi05MjRkLWY2ODZjMGFmM2JiZCJ9'
                         '.ENWLKyzZcmGGpHF4_kr6kuOkw8X9UObOPtbWaDuV0qWFvIvEZ699pXqnWCfh3PGcRIoH1IIeXu05ux5TJG8uBg',
        'User-Agent': 'Apifox/1.0.0 (https://apifox.com)',
        'Content-Type': 'application/json',
        'Accept': '*/*',
        'Host': 'localhost:9090',
        'Connection': 'keep-alive'
    }

    response = requests.request("POST", url, headers=headers, data=payload, timeout=50)
    success.append(car_type)
    print(f'同步完成 共{len(car_types)}条->同步到第{num}条->当前车型{car_type}->同步结果{response.text}')


def main():
    read_car_type()
    read_cookie_file()

    debug()

    suc_set = set(success)
    error_car_type = [item for item in car_types if item not in suc_set]
    print(f'同步错误车型:\n{error_car_type}')  # 输出将会是 [1, 2, 5]


main()
