from django.db import models


# Create your models here.
# t_user 表
class t_user(models.Model):
    # 限定字段长度
    # 字段名 name，字段 char（相当于varchar）类型
    id = models.AutoField(primary_key=True)
    username = models.CharField(max_length=255)
    password = models.CharField(max_length=255)
